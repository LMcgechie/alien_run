﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace alien_run
{
    class Player
    {
        /*------------------------------
        Data
        ------------------------------*/
        private Vector2 position = Vector2.Zero,
                        velocity = Vector2.Zero,
                        prevPosition = Vector2.Zero;
        private Texture2D sprite = null;
        private Level ourLevel = null;
        private bool isGrounded = false, jumpLaunch = false;
        private float jumpButtonTime = 0f;
               

        //Constants
        private const float VELOCITY_X = 300.0f;
        private const float GRAV_ACCEL = 3400.0f;
        private const float TERM_VEL = 550.0f;
        private const float JUMP_LAUNCH_VEL = -3000.0f;
        private const float MAX_JUMP_TIME = 0.3f;
        private const float JUMP_CONTROLL_POWER = 0.75f;

        /*------------------------------
        Methods
        ------------------------------*/

        public Player(Level newLevel)
        {
            ourLevel = newLevel;
        }

        public void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>("player/player-stand");
        }

        public void Update(GameTime gameTime)
        {
            //Get delta time
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Update velocity based on input, gravity, etc
            //Horizontal velocity = constant
            velocity.X = VELOCITY_X;

            velocity.Y += GRAV_ACCEL * deltaTime;
            //Clamp vertical velocity to termical speed
            velocity.Y = MathHelper.Clamp(velocity.Y, -TERM_VEL, TERM_VEL);


            //Jump check apply tpo velocity
            Input(gameTime);

            //Update position
            //pos2 = pos1 + delta pos
            //deltapos = velocity * deltatime
            prevPosition = position;
            position += velocity * deltaTime;

            //Check if colliding
            CheckTileCollision();
        }

        private void CheckTileCollision()
        {
            //assuming not touching ground this frame
            isGrounded = false;

            Rectangle playerBounds = GetBounds();
            Rectangle prevPlayerBounds = GetPrevBounds();

            //Get list of tiles from level
            List<Tile> collidingTiles = ourLevel.GetTilesInBounds(playerBounds);

            //For collision move player out of tile
            foreach (Tile collidingTile in collidingTiles)
            {
                //Determine how far overlap is
                Rectangle tileBounds = collidingTile.GetBounds();
                Vector2 depth = GetCollisionDepth(tileBounds, playerBounds);
                Tile.TileType tileType = collidingTile.GetTileType();
                

                if (depth != Vector2.Zero)
                {
                    float absDepthX = Math.Abs(depth.X),
                          absDepthY = Math.Abs(depth.Y);

                    //Resolve collision along shallowest axes
                    if (absDepthY < absDepthX)
                    {

                        bool fallOnTile = playerBounds.Bottom > tileBounds.Top && prevPlayerBounds.Bottom <= tileBounds.Top;

                        if (tileType == Tile.TileType.IMPASSABLE || (tileType == Tile.TileType.PLATFORM && fallOnTile))
                        {
                            //Y is shallowest, thus resolve
                            position.Y += depth.Y;
                            playerBounds = GetBounds();

                            if (playerBounds.Bottom >= tileBounds.Top)
                                isGrounded = true;
                        }

                        if (playerBounds.Bottom >= tileBounds.Top)
                            isGrounded = true;
                    }
                    else if (tileType == Tile.TileType.IMPASSABLE)
                    {
                        position.X += depth.X;
                        playerBounds = GetBounds();
                        
                    }
                }
            }
        }

        private Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }

        private Rectangle GetPrevBounds()
        {
            return new Rectangle((int)prevPosition.X, (int)prevPosition.Y, sprite.Width, sprite.Height);
        }

        private Vector2 GetCollisionDepth(Rectangle tile, Rectangle player)
        {
            // Calculate how far player overlaps tiles

            //Calc half sizes of both rects
            float halfWidthPlayer = player.Width / 2.0f,
                  halfHeightPlayer = player.Height / 2.0f,
                  halfWidthTile = tile.Width / 2.0f,
                  halfHeightTile = tile.Height / 2.0f;

            //Calc centre of rects
            Vector2 centrePlayer = new Vector2(player.Left + halfWidthPlayer, 
                                               player.Top + halfHeightPlayer),
                    centreTile = new Vector2(tile.Left + halfWidthTile,
                                             tile.Top + halfHeightTile);

            //How far away are centres
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            //Minimum distance need to not collide
            //If X or Y is > than ^^^^ then not colliding
            float minDistanceX = halfWidthPlayer + halfWidthTile;
            float minDistanceY = halfHeightPlayer + halfWidthPlayer;

            //If not intersecting return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
                return Vector2.Zero;

            //Calc and return intersection depth
            float depthX, depthY;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;

            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }

        private void Input(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();

            bool allowedToJump = isGrounded == true || (jumpLaunch == true && jumpButtonTime <= MAX_JUMP_TIME);

            //Check for trying to jump
            if (keyState.IsKeyDown(Keys.Space) && isGrounded == true)
            {
                jumpLaunch = true;
                jumpButtonTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

                //Trying to jump
                velocity.Y = JUMP_LAUNCH_VEL * (1.0f - (float)Math.Pow(jumpButtonTime / MAX_JUMP_TIME, JUMP_CONTROLL_POWER));

                //Scale jump based on length of press

            }

            else
            {
                jumpLaunch = false;
                jumpButtonTime = 0;
            }
        }

        public Vector2 getPosition()
        {
            return position;
        }
    }
}
