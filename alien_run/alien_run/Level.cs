﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace alien_run
{
    class Level
    {
        /*------------------------------
        Data
        ------------------------------*/
        private Tile[,] tiles;
        private Texture2D boxSprite, bridgeSprite;

        /*------------------------------
        constant
        ------------------------------*/
        private const int LEVEL_WIDTH = 20, LEVEL_HEIGHT = 20;
        private const int tileHeight = 70, tileWidth = 70;


        /*------------------------------
        Methods
        ------------------------------*/

        public void LoadContent(ContentManager content)
        {
            //creating a single copy of the tile texture for all tiles
            boxSprite = content.Load<Texture2D>("tiles/box");
            bridgeSprite = content.Load<Texture2D>("tiles/bridge");
            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            //Create box
            createBox(0, 7);
            createBox(1, 7);
            createBox(2, 7);
            createBox(3, 7);
            createBox(4, 7);
            createBox(5, 7);
            createBox(6, 7);
            createBox(7, 7);


            //Create bridge
            createBridge(0, 5);
            createBridge(1, 5);
            createBridge(2, 5);
            createBridge(3, 5);
            createBridge(4, 5);
            createBridge(5, 5);

        }

        public void createBox(int x,int y)
        {            
            Vector2 tilePosition = new Vector2(x * tileWidth, y * tileHeight);
            Tile newTile = new Tile(boxSprite, tilePosition, Tile.TileType.IMPASSABLE);
            tiles[x, y] = newTile;                
        }

        public void createBridge(int x, int y)
        {
            Vector2 tilePosition = new Vector2(x * tileWidth, y * tileHeight);
            Tile newTile = new Tile(bridgeSprite, tilePosition, Tile.TileType.PLATFORM);
            tiles[x, y] = newTile;
        }


        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            //Creating empty list for tiles
            List<Tile> tilesInBounds = new List<Tile>();

            //Determin tile coordinate rage
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth),
                rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1,
                topTile = (int)Math.Floor((float)bounds.Top / tileHeight),
                bottomTile = (int)Math.Ceiling ((float)bounds.Bottom / tileWidth) - 1;

            //Loop through ^^^^ List
            for (int x = leftTile; x <= rightTile; x++)
                for (int y = topTile; y <= bottomTile; y++)
                {
                    if (GetTile(x, y) != null)
                        tilesInBounds.Add(tiles[x, y]);
                }
                    return tilesInBounds;
        }

        public Tile GetTile(int x, int y)
        {
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null;

            return tiles[x, y];
        }


        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; x++)
                for (int y = 0; y < LEVEL_HEIGHT; y++)
                {
                    if (tiles[x,y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
                    
        }
    }
}
