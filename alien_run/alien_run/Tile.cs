﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;


namespace alien_run
{
    class Tile
    {

        /*------------------------------
        Enum
        ------------------------------*/
        public enum TileType
        {
            IMPASSABLE,
            PLATFORM
        }

        /*------------------------------
        Data
        ------------------------------*/
        private Texture2D sprite;
        private Vector2 position;
        private TileType tileType;

        /*------------------------------
        Methods
        ------------------------------*/
        public Tile(Texture2D newSprite, Vector2 newPosition, TileType newType)
        {
            sprite = newSprite;
            position = newPosition;
            tileType = newType;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }

        public Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }

        public TileType GetTileType()
        {
            return tileType;
        }
    }
}
