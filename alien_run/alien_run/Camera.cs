﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace alien_run
{
    class Camera
    {
        /*------------------------------
        Data
        ------------------------------*/
        private Vector2 viewSize;
        private Vector2 position;
        Player target;


        /*------------------------------
        Methods
        ------------------------------*/
        public Camera(Player newTarget, Vector2 newViewSize)
        {
            target = newTarget;
            viewSize = newViewSize;
        }

        public void Update()
        {
            position.X = target.getPosition().X - 0.5f * viewSize.X;
        }

        public void Begin(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(transformMatrix: Matrix.CreateTranslation(-position.X, -position.Y, 0));
        }
    }
}
